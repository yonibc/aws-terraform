To execute this code, first we must install terraform in our pc. To install terraform: https://learn.hashicorp.com/tutorials/terraform/install-cli
We also need to install AWS Cli and configure our credentials.
After iinstalling AWS Cli, we can configure our credentials locally with:

aws configure

and following the prompt.

When everything is done, open a console located in our terraform proyect and execute:

terraform init
terraform apply

We could check if our code is valid with:   terraform validate
This will check if the structure of the code is valid (some clasp unclosed, missing arguments, etc)